#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>

#include "ppos_data.h"

#define STACKSIZE 32768		/* tamanho de pilha das threads */

static task_t *mainTask;
static task_t *actualTask;

static int idTask = 1; 

void ppos_init() {
	setvbuf (stdout, 0, _IONBF, 0) ;
	mainTask = malloc(sizeof(task_t));
	mainTask->next = mainTask->prev = NULL;
	mainTask->id = 0;
	actualTask = mainTask;
	getcontext(&(mainTask->context));
}

int task_create (task_t *task, void (*start_func)(void *), void *arg) {
	task->next = task->prev = NULL;
	task->id = idTask++;

	#ifdef DEBUG
	printf("Cria tarefa de id: %d\n",task->id);
	#endif

	getcontext(&(task->context)) ;
  	char *stack = malloc(STACKSIZE) ;
   	if (stack) {
	      	task->context.uc_stack.ss_sp = stack ;
	      	task->context.uc_stack.ss_size = STACKSIZE ;
	      	task->context.uc_stack.ss_flags = 0 ;
	      	task->context.uc_link = 0 ;
   	} else {
	    	perror ("Erro na criação da pilha: ") ;
	    	return -1;
   	}
   	makecontext(&(task->context), (void *)(*start_func), 1, arg) ;
	return task->id;
}

int task_switch (task_t *task) {
	#ifdef DEBUG
	printf("Muda de tarefa %d para %d\n",actualTask->id,task->id);
	#endif

	task_t *t = actualTask;
	actualTask = task;
	return swapcontext (&(t->context), &(task->context));
}

void task_exit (int exit_code) {
	#ifdef DEBUG
	printf("Tarefa %d terminada\n",actualTask->id);
	#endif

	task_t *t = actualTask;
	actualTask = mainTask;
	swapcontext (&(t->context), &(mainTask->context));
}

int task_id () {
	return actualTask->id;
}








